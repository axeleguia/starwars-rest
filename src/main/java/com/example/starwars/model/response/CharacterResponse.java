package com.example.starwars.model.response;

import java.util.List;

import com.example.starwars.model.CharacterDTO;

public class CharacterResponse {

	int count;
	String next;
	String previous;
	List<CharacterDTO> results;

	public int getCount() {
		return count;
	}

	public String getNext() {
		return next;
	}

	public String getPrevious() {
		return previous;
	}

	public List<CharacterDTO> getResults() {
		return results;
	}
	
}
