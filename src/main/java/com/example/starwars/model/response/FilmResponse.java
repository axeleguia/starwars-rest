package com.example.starwars.model.response;

import java.util.List;

import com.example.starwars.model.FilmDTO;

public class FilmResponse {

	int count;
	String next;
	String previous;
	List<FilmDTO> results;

	public int getCount() {
		return count;
	}

	public String getNext() {
		return next;
	}

	public String getPrevious() {
		return previous;
	}

	public List<FilmDTO> getResults() {
		return results;
	}
	
}
