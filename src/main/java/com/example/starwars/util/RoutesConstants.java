package com.example.starwars.util;

public final class RoutesConstants {

	public static final String GET_ROOT = "https://swapi.dev/api/";
	public static final String GET_FILMS = "https://swapi.dev/api/films/";
	public static final String GET_PEOPLE = "https://swapi.dev/api/people/";
	
}
