package com.example.starwars.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.starwars.model.FilmDTO;
import com.example.starwars.model.response.FilmResponse;
import com.example.starwars.util.RoutesConstants;

@RestController
@RequestMapping("/peliculas")
public class FilmController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("")
	public List<FilmDTO> getFilms() {
		
		List<FilmDTO> filmList = new ArrayList<FilmDTO>();
		
		try {
			// Get films
			FilmResponse filmResponse = restTemplate.getForObject(RoutesConstants.GET_FILMS, FilmResponse.class);
			
			// Assign id to each film
			for(int i = 0; i< filmResponse.getResults().size();i++) {
				filmResponse.getResults().get(i).setId(i+1);
			}

			filmList.addAll(filmResponse.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filmList;
	}

}
