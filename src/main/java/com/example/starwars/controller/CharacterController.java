package com.example.starwars.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.starwars.model.CharacterDTO;
import com.example.starwars.model.response.CharacterResponse;
import com.example.starwars.util.RoutesConstants;

@RestController
public class CharacterController {

	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping(value = "/personajes", method = RequestMethod.GET)
	public List<CharacterDTO> getCharacters() {

		List<CharacterDTO> characterList = new ArrayList<CharacterDTO>();

		try {
			CharacterResponse response = restTemplate.getForObject(RoutesConstants.GET_PEOPLE, CharacterResponse.class);

			// Extract id from url
			if (response.getResults().size() != 0) {
				response.getResults().forEach(result -> {
					Pattern pattern = Pattern.compile("people\\/([0-9]+)");
					Matcher matcher = pattern.matcher(result.getUrl());
					while (matcher.find()) {
						String characterId = matcher.group(1);
						result.setCharacterId(characterId);
					}

				});

				characterList.addAll(response.getResults());
			}

			while (response.getNext() != null) {
				String page = response.getNext().substring(response.getNext().lastIndexOf("=") + 1);
				response = restTemplate.getForObject(RoutesConstants.GET_PEOPLE + "?page={page}",
						CharacterResponse.class, page);
				if (response.getResults().size() != 0) {
					response.getResults().forEach(result -> {
						Pattern pattern = Pattern.compile("people\\/([0-9]+)");
						Matcher matcher = pattern.matcher(result.getUrl());
						while (matcher.find()) {
							String characterId = matcher.group(1);
							result.setCharacterId(characterId);
						}

					});
					characterList.addAll(response.getResults());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return characterList;
	}

	@RequestMapping(value = "/personajes/{characterId}", method = RequestMethod.GET)
	public CharacterDTO getCharacterById(@PathVariable String characterId) {

		CharacterDTO response = null;
		try {
			response = restTemplate.getForObject("http://swapi.dev/api/people/{characterId}" + characterId,
					CharacterDTO.class, characterId);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
