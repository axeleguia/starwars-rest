Evaluación de candidato:

Usando tecnología Java Web, usando los frameworks de tu preferencia, se requiere:

Usando el API https://swapi.dev/

Se debe crear una aplicación que permita visualizar todas las películas de Star War. (https://swapi.dev/api/films/)

Se debe poder ver información de las películas, así como la opción de Ver Actores (characters). Cuando se ingrese a la vista de actores, se debe visualizar la información del actor así como la lista de las películas de la saga donde participó.

Se evaluará:
- Diseño de aplicación y experiencia de usuario
- Análisis de despliegue de la información
- Codificación, estándares, patrones de diseños y el uso de frameworks y librerías empleados para la solución presentada
- El código debe ser subido al repositorio de GitLab para revisar el historial de commits realizados.W